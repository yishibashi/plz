import «Plz»


def main : IO Unit := do
  let mut env : CalcVar.Env := CalcVar.empty_env
  while true do
    IO.print "calc_var> "
    let r <- (<- IO.getStdin).getLine
    match r.parse? CalcVar.parseCommand with
    | some e =>
      let (result, env') := e.eval env
      if let some v := result then
        IO.println s!"{v}"
      env := env'
    | none =>
      IO.println s!"parse error: {r}"
