
namespace Calc

inductive Expr where
-- ..., -2, -1, 0, 1, 2, ...
| numeral : Int → Expr
-- -e
| negate : Expr → Expr
-- e₁ + e₂
| plus : Expr → Expr → Expr
-- e₁ - e₂
| minus : Expr → Expr → Expr
-- e₁ * e₂
| times : Expr → Expr → Expr
-- e₁ / e₂
| divide : Expr → Expr → Expr
deriving Inhabited, Repr

def Expr.toString (e : Expr) : String := toStr 0 e
where
  toStr (n : Nat) (e : Expr) : String :=
    let (m, s) := match e with
    | numeral n    => if (0 ≤ n) then
                        (4, ToString.toString (Int.toNat n))
                      else
                        (4, "-"++(ToString.toString (Int.toNat (-n))))
    | negate e     => (3, "-" ++ (toStr 3 e))
    | times e₁ e₂  => (2, (toStr 2 e₁) ++ " * " ++ (toStr 3 e₂))
    | divide e₁ e₂ => (2, (toStr 2 e₁) ++ " / " ++ (toStr 3 e₂))
    | plus e₁ e₂   => (1, (toStr 1 e₁) ++ " + " ++ (toStr 2 e₂))
    | minus e₁ e₂  => (1, (toStr 1 e₁) ++ " - " ++ (toStr 2 e₂))
  if (m < n) then "(" ++ s ++ ")" else s
instance : ToString Expr where
  toString e := Expr.toString e

def Expr.eval (e : Expr) : Int :=
match e with
| numeral n    => n
| negate e     => - (e.eval)
| plus e₁ e₂   => e₁.eval + e₂.eval
| minus e₁ e₂  => e₁.eval - e₂.eval
| times e₁ e₂  => e₁.eval * e₂.eval
| divide e₁ e₂ => e₁.eval / e₂.eval

declare_syntax_cat CALC
syntax num : CALC
syntax:60 CALC:60 "+" CALC:61 :CALC
syntax:60 CALC:60 "-" CALC:61 :CALC
syntax:70 CALC:70 "*" CALC:71 :CALC
syntax:70 CALC:70 "/" CALC:71 :CALC
syntax:80 "-"CALC:80 :CALC
syntax "(" CALC ")" : CALC
syntax "`[calc> " CALC "]" : term
macro_rules
  | `(`[calc> $num:num]) => `(Expr.numeral $num)
  | `(`[calc> -$x]) => `(Expr.negate `[calc> $x])
  | `(`[calc> $x + $y]) => `(Expr.plus   `[calc> $x] `[calc> $y])
  | `(`[calc> $x - $y]) => `(Expr.minus  `[calc> $x] `[calc> $y])
  | `(`[calc> $x * $y]) => `(Expr.times  `[calc> $x] `[calc> $y])
  | `(`[calc> $x / $y]) => `(Expr.divide `[calc> $x] `[calc> $y])
  | `(`[calc> ($x)]) => `(`[calc> $x])

syntax "`[calc>> " CALC "]" : term
macro_rules
  | `(`[calc>> $e]) => `(Expr.eval `[calc> $e])

section examples

#eval `[calc>> 2+2]

#eval `[calc>> 101 * 111]

-- divition by 0 of Int = 0
-- see, Basic.lean
#eval `[calc>> 24 / (4-4)]

end examples


end Calc
