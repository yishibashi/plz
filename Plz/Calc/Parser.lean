import Plz.Utils
import Plz.Calc.Calc
import Lean.Data.Parsec

open Lean Parsec


namespace Calc

def check (parser : Parsec Expr) (input : String) (expected : Int) := Id.run do
  match input.parse? parser with
  | some v =>
    if (v.eval == expected) then
      dbg_trace s!"ok, {input}={expected}"
      true
    else
      dbg_trace s!"fail, {input}={v.eval}, but expected {expected}"
      false
  | none =>
    dbg_trace s!"parse fail:{input} (expected value={expected})"
    false

def dbg_check (parser : Parsec Expr) (input : String) := input.dbg_parse? parser

def parseNumeral' := attempt do
  let c <- peek!
  if (c == '-') then
    skip
    let d <- natDigit
    pure $ Calc.Expr.numeral (Int.negOfNat d)
  else if ('0' ≤ c ∧ c ≤ '9') then
    let d <- natDigit
    pure $ Calc.Expr.numeral (Int.ofNat d)
  else
    fail s!"parsing Numeral failed: {c}"
partial def parseNumeral := attempt do
 (attempt do
    let t <- ws *> pchar '(' *> ws *> parseNumeral <* ws <* pchar ')' <* ws
    pure t
  )
  <|>
  (attempt do
    let t <- ws *> parseNumeral' <* ws
    pure t
  )
  <|>
  parseNumeral'
  <|>
  (attempt do
    let t <- ws *> pchar '-' *> parseNumeral <* ws
    pure $ Calc.Expr.negate t
  )

mutual
partial def parseTerm  := attempt do
  let lhs <- parseFactor
  let rhss <- many $
    (attempt do
      let rhs <- pchar '*' *> parseFactor
      pure $ (λ lhs => Calc.Expr.times lhs rhs)
    )
    <|>
    (attempt do
      let rhs <- pchar '/' *> parseFactor
      pure $ (λ lhs => Calc.Expr.divide lhs rhs)
    )
  pure $ Array.foldl (λ x f => f x) lhs rhss

partial def parseExpr := attempt do
  let lhs <- parseTerm
  let rhss <- many $
    (attempt do
      let _ <- pchar '+'
      let rhs <- parseTerm
      pure $ (λ lhs => Calc.Expr.plus lhs rhs)
    )
    <|>
    (attempt do
      let _ <- pchar '-'
      let rhs <- parseTerm
      pure $ (λ lhs => Calc.Expr.minus lhs rhs)
    )
  pure $ Array.foldl (λ x f => f x) lhs rhss

partial def parseFactor := attempt do
  (attempt do
    pure $ <- ws *> pchar '(' *> ws *> parseExpr <* ws <* pchar ')' <* ws
  )
  <|>
  parseNumeral
end

section test_parse_expr
  #eval check parseExpr "123" (123)
  #eval check parseExpr " 123" (123)
  #eval check parseExpr " 123 " (123)
  #eval check parseExpr "(123)" (123)
  #eval check parseExpr "-123" (-123)
  #eval check parseExpr " -123" (-123)
  #eval check parseExpr " -123 " (-123)
  #eval check parseExpr "(-123)" (-123)
  #eval check parseExpr "-123 " (-123)
  #eval check parseExpr " -123 " (-123)
  #eval check parseExpr " -123 " (-123)
  #eval check parseExpr " ( -123 ) " (-123)
  #eval check parseExpr " ( -123 ) " (-123)
  #eval check parseExpr "( --123 )" (- -123)
  #eval check parseExpr "-( -123 )" (- -123)
  #eval check parseExpr " - ( -123 )" (- -123)
  #eval check parseExpr " - (  -123 " (- -123)
  -- fail
  #eval check parseExpr " -   -123 )" (- -123)

  #eval check parseExpr "1*2" (1*2)
  #eval check parseExpr "1* 2 " (1*2)
  #eval check parseExpr " 1* 2 " (1*2)
  #eval check parseExpr " 1 * 2" (1*2)
  #eval check parseExpr "1*2" (1*2)
  #eval check parseExpr "1* 2 " (1*2)
  #eval check parseExpr " 1* 2 " (1*2)
  #eval check parseExpr " 1 * 2" (1*2)
  #eval check parseExpr "  (1)*2 " (1*2)
  #eval check parseExpr "  1* 2  " (1*2)
  #eval check parseExpr "   1* 2  " (1*2)
  #eval check parseExpr "   1 * 2 " (1*2)
  #eval check parseExpr "  1*2 " (1*2)
  #eval check parseExpr "  1* 2  " (1*2)
  #eval check parseExpr "   (1)* ((2))  " (1*2)
  #eval check parseExpr "   1 * 2 " (1*2)
  -- fail
  #eval check parseExpr "1 * 2)" (1*2)
  #eval check parseExpr "(   1 * 2)" (1*2)
  #eval check parseExpr "( (  1 * 2))" (1*2)

  #eval check parseExpr "1*2 + 3" (1*2 + 3)
  #eval check parseExpr "1*2 + 3/4" (1*2 + 3/4)
  #eval check parseExpr "1*2 + 3/4 -5" (1*2 + 3/4 -5)
  #eval check parseExpr "1*2 + 3/4 -5 + 6/2" (1*2 + 3/4 -5 + 6/2)
  #eval check parseExpr "(1*2) -3" ((1*2) - 3)
  #eval check parseExpr "(1*2) -(-3 / 1) +1 " ((1*2) -(-3 / 1) +1)

end test_parse_expr





end Calc
