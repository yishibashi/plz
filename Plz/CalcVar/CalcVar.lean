import Std.Data.HashMap

namespace CalcVar

inductive Expr where
-- x,y,z,..
| var : String → Expr
-- ..., -2, -1, 0, 1, 2, ...
| numeral : Int → Expr
-- -e
| negate : Expr → Expr
-- e₁ + e₂
| plus : Expr → Expr → Expr
-- e₁ - e₂
| minus : Expr → Expr → Expr
-- e₁ * e₂
| times : Expr → Expr → Expr
-- e₁ / e₂
| divide : Expr → Expr → Expr
deriving Inhabited, Repr

inductive Command where
-- e
| expression : Expr → Command
-- x := e
| definition : String → Expr → Command
deriving Inhabited, Repr

def Expr.toString (e : Expr) : String := toStr 0 e
where
  toStr (n : Nat) (e : Expr) : String :=
    let (m, s) := match e with
    | var s        => (4, s)
    | numeral n    => if (0 ≤ n) then
                        (4, ToString.toString (Int.toNat n))
                      else
                        (4, "-"++(ToString.toString (Int.toNat (-n))))
    | negate e     => (3, "-" ++ (toStr 3 e))
    | times e₁ e₂  => (2, (toStr 2 e₁) ++ " * " ++ (toStr 3 e₂))
    | divide e₁ e₂ => (2, (toStr 2 e₁) ++ " / " ++ (toStr 3 e₂))
    | plus e₁ e₂   => (1, (toStr 1 e₁) ++ " + " ++ (toStr 2 e₂))
    | minus e₁ e₂  => (1, (toStr 1 e₁) ++ " - " ++ (toStr 2 e₂))
  if (m < n) then "(" ++ s ++ ")" else s
instance : ToString Expr where
  toString e := Expr.toString e

def Command.toString (c : Command) : String :=
match c with
| expression e => s!"{e}"
| definition s e => s!"{s} := {e}"
instance : ToString Command where
  toString c := Command.toString c

abbrev Env := Std.HashMap String Expr
def empty_env : Env := Std.HashMap.empty

partial
def Expr.eval (e : Expr)  (env : Env) := do
match e with
| var s => if let some e := env.find? s then
              (e.eval env)
            else
              dbg_trace s!"unkown variable: {s}"
              none
| numeral n    => some n
| negate e     => (- ·) <*> (e.eval env)
| plus e₁ e₂   => (· + ·) <$> (e₁.eval env) <*> (e₂.eval env)
| minus e₁ e₂  => (· - ·) <$> (e₁.eval env) <*> (e₂.eval env)
| times e₁ e₂  => (· * ·) <$> (e₁.eval env) <*> (e₂.eval env)
| divide e₁ e₂ => (· / ·) <$> (e₁.eval env) <*> (e₂.eval env)

partial
def Command.eval (c : Command) (env : Env) := Id.run do
match c with
| expression e => (e.eval env, env)
| definition s e => (none, env.insert s e)

partial
def Command.eval' (c : Command) := c.eval Std.HashMap.empty



end CalcVar
