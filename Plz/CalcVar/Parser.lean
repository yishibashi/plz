import Plz.Utils
import Plz.CalcVar.CalcVar
import Lean.Data.Parsec

open Lean Parsec


namespace CalcVar


def parseId :=
  many1Chars $ satisfy (λ c => ('A' ≤ c ∧ c ≤ 'Z') ∨ ('a' ≤ c ∧ c ≤ 'z'))
def parseNumeral' := attempt do
  let c <- peek!
  if (c == '-') then
    skip
    let d <- natDigit
    pure $ CalcVar.Expr.numeral (Int.negOfNat d)
  else if ('0' ≤ c ∧ c ≤ '9') then
    let d <- natDigit
    pure $ CalcVar.Expr.numeral (Int.ofNat d)
  else
    let v <- parseId
    pure $ CalcVar.Expr.var v
partial def parseNumeral := attempt do
 (attempt do
    let t <- ws *> pchar '(' *> ws *> parseNumeral <* ws <* pchar ')' <* ws
    pure t
  )
  <|>
  (attempt do
    let t <- ws *> parseNumeral' <* ws
    pure t
  )
  <|>
  parseNumeral'
  <|>
  (attempt do
    let t <- ws *> pchar '-' *> parseNumeral <* ws
    pure $ CalcVar.Expr.negate t
  )

mutual
partial def parseTerm  := attempt do
  let lhs <- parseFactor
  let rhss <- many $
    (attempt do
      let rhs <- pchar '*' *> parseFactor
      pure $ (λ lhs => CalcVar.Expr.times lhs rhs)
    )
    <|>
    (attempt do
      let rhs <- pchar '/' *> parseFactor
      pure $ (λ lhs => CalcVar.Expr.divide lhs rhs)
    )
  pure $ Array.foldl (λ x f => f x) lhs rhss

partial def parseExpr := attempt do
  let lhs <- parseTerm
  let rhss <- many $
    (attempt do
      let _ <- pchar '+'
      let rhs <- parseTerm
      pure $ (λ lhs => CalcVar.Expr.plus lhs rhs)
    )
    <|>
    (attempt do
      let _ <- pchar '-'
      let rhs <- parseTerm
      pure $ (λ lhs => CalcVar.Expr.minus lhs rhs)
    )
  pure $ Array.foldl (λ x f => f x) lhs rhss

partial def parseFactor := attempt do
  (attempt do
    pure $ <- ws *> pchar '(' *> ws *> parseExpr <* ws <* pchar ')' <* ws
  )
  <|>
  parseNumeral
end

partial
def parseCommand := attempt do
  (attempt do
    -- expression
    let e <- parseExpr <* eof
    pure $ CalcVar.Command.expression e
  )
  <|>
  (attempt do
    -- definition
    let s <- parseId <* pstring ":="
    let e <- parseExpr <* eof
    pure $ CalcVar.Command.definition s e
  )

end CalcVar
