import Plz.MiniML.Machine
import Plz.MiniML.MiniML

namespace MiniML
open Expr
open MiniML.Machine.Instruction

def compile (e : Expr) :=
match e with
| .EVar n => [IVar n]
| .EInt i => [IInt i]
| .EBool b => [IBool b]
| .EPlus e₁ e₂ => (compile e₁) ++ (compile e₂) ++ [IAdd]
| .ETimes e₁ e₂ => (compile e₁) ++ (compile e₂) ++ [IMult]
| .EMinus e₁ e₂ => (compile e₁) ++ (compile e₂) ++ [ISub]
| .EEq e₁ e₂ => (compile e₁) ++ (compile e₂) ++ [IEqual]
| .ELt e₁ e₂ => (compile e₁) ++ (compile e₂) ++ [ILess]
| .EIf e₁ e₂ e₃ => (compile e₁) ++ [IBranch (compile e₂) (compile e₃)]
| .EFun f x _ e _ => [IClosure f x ((compile e) ++ [IPopEnv])]
| .EApply e₁ e₂ => (compile e₁) ++ (compile e₂) ++ [ICall]



section test

#check factorial_5
def compiled_factorial_5 := compile factorial_5
#check compiled_factorial_5
#eval compiled_factorial_5

#eval Machine.run (compile factorial_5) []

end test

end MiniML
