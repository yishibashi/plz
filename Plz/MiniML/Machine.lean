import Plz.MiniML.MiniML


namespace MiniML.Machine

mutual

inductive MValue where
| MInt : Int → MValue
| MBool : Bool → MValue
| MClosure : Name → List Instruction → List (Name × Thunk MValue) → MValue
deriving Inhabited

inductive Instruction where
| IMult : Instruction --  multiplication
| IAdd : Instruction --  addition
| ISub : Instruction --  subtraction
| IEqual : Instruction --  equality
| ILess : Instruction --  less than
| IVar : Name → Instruction --  push value of variable
| IInt : Int  → Instruction --  push integer constant
| IBool : Bool → Instruction --  push boolean constant
| IClosure : Name → Name → List Instruction → Instruction --  push closure
| IBranch : List Instruction → List Instruction → Instruction --  branch
| ICall --  execute a closure
| IPopEnv --  pop environment
deriving Inhabited
end

abbrev Frame := List Instruction
abbrev Environment := List (Name × Thunk MValue)
abbrev Stack := List (Thunk MValue)

instance : ToString MValue where
  toString mv :=
    match mv with
    | .MInt i => s!"{i}"
    | .MBool b => s!"{b}"
    | .MClosure n _ _ => s!"<fun:{n}>"

partial
def Instruction.toString' (inst : Instruction) : String :=
match inst with
| .IMult => "IMult"
| .IAdd => "IAdd"
| .ISub => "ISub"
| .IEqual=> "IEqual"
| .ILess => "ILess"
| .IVar n => s!"IVar {n}"
| .IInt i => s!"IInt {i}"
| .IBool b => s!"IBool {b}"
| .IClosure f n frm  => s!"IClosure {f} {n}" ++ frm.foldl (λ e => · ++ ((toString' e) ++ ", ")) ""
| .IBranch e₁ e₂  =>
  s!"IBranch "
  ++ e₁.foldl (λ e => · ++ ((toString' e) ++ ", ")) ""
  ++ e₂.foldl (λ e => · ++ ((toString' e) ++ ", ")) ""
| .ICall => "ICall"
| .IPopEnv => "IPopEnv"
instance : ToString Instruction where
  toString inst := inst.toString'

def lookup (n : Name) (envs : List Environment) := do
  match envs with
  | env :: _ => {
  if let some v := env.find? (λ (e : Name × Thunk MValue) => e.fst == n) then
    Except.ok v.snd.get
  else
    Except.error s!"Unkown {n}."
  }
  | _ => Except.error "Environment Empty. Unkown {n}."

def pop (s : Stack) := Id.run do
match s with
| [] => Except.error "Empty Stack."
| h::tl => Except.ok (h, tl)

def pop_bool (s : Stack) :=
match s with
| h :: tl =>
  match h.get with
  | (.MBool b) => Except.ok (b,tl)
  | _ => Except.error "Expected Bool."
| _ => Except.error "Empty Stack. Expected Bool."

def pop_app (s : Stack) :=
match s with
-- | v :: (.MClosure x f e) :: s => Except.ok (x, f, e, v, s)
| v :: c :: s =>
  match c.get with
  | (.MClosure x f e) => Except.ok (x, f, e, v.get, s)
  | _ => Except.error "Expected Value & Closure."
| _ => Except.error "Empty Stack. Expected Value & Closure."

def add (s : Stack) : Except String Stack :=
match s with
| x :: y :: s =>
  match x.get, y.get with
  |(.MInt x), (.MInt y) => pure $ MValue.MInt (y+x) :: s
  | _, _ => Except.error "Expected Add Int Int."
| _ => Except.error "Empty Stack. Expected Add Int Int."

def mult (s : Stack) : Except String Stack :=
match s with
| x :: y :: s =>
  match x.get, y.get with
  | (.MInt x), (.MInt y) => pure $ MValue.MInt (y*x) :: s
  | _, _ => Except.error "Expected Multi Int Int."
| _ => Except.error "Empty Stack. Expected Multi Int Int."

def subst (s : Stack) : Except String Stack :=
match s with
| x :: y :: s =>
  match x.get, y.get with
  | (.MInt x), (.MInt y) => pure $ MValue.MInt (y-x) :: s
  | _, _ => Except.error "Expected Subst Int Int."
| _ => Except.error "Empty Stack. Expected Subst Int Int."

def eq (s : Stack) : Except String Stack :=
match s with
| x :: y :: s =>
  match x.get, y.get with
  | (.MInt x), (.MInt y) => pure $ MValue.MBool (y=x) :: s
  | _, _ => Except.error "Expected Eq Int Int."
| _ => Except.error "Empty Stack. Expected Eq Int Int."

def lt (s : Stack) : Except String Stack :=
match s with
| x :: y :: s =>
  match x.get, y.get with
  | (.MInt x), (.MInt y) => pure $ MValue.MBool (y<x) :: s
  | _, _ => Except.error "Expected Lt Int Int."
| _ => Except.error "Empty Stack. Expected Lt Int Int."


partial
def exec (instr : Instruction) (frms : List Frame) (stck : Stack) (envs : List Environment) : Except String ((List Frame) × Stack × (List Environment)) := do
match instr with
| .IAdd => pure (frms, (<- add stck), envs)
| .IMult => pure (frms, (<- mult stck), envs)
| .ISub => pure (frms, (<- subst stck), envs)
| .IEqual => pure (frms, (<- eq stck), envs)
| .ILess => pure (frms, (<- lt stck), envs)
| .IVar x => pure (frms, (<- lookup x envs) :: stck, envs)
| .IInt i => pure (frms, (Thunk.mk (λ _ => (.MInt i))) :: stck, envs)
| .IBool b => pure (frms, (Thunk.mk (λ _ => (.MBool b))) :: stck, envs)
| .IClosure f x frm =>
  match envs with
  | env :: _ =>
    let rec c := (MValue.MClosure x frm ((f, c) :: env))
    pure (frms,  c :: stck, envs)
  | _ => Except.error s!"failed: `exec IClosure {f} {x} {frm}` with no environment"
| .IBranch (f1 : Frame) (f2 : Frame) =>
  let (b, stck') <- pop_bool stck
  pure ((if b then f1 else f2) :: frms, stck', envs)
| .ICall =>
  let (x, frm, env, v, stck') <- pop_app stck
  pure (frm :: frms, stck', ((x,v) :: env) :: envs)
| .IPopEnv => (
  match envs with
  | [] => Except.error "Empty Environment. PopEnv failed."
  | _ :: envs' => pure (frms, stck, envs'))


partial
def run (frm : Frame) (env : Environment) := do
  loop [frm] [] [env]
where
  loop (frms : List Frame) (stck : Stack) (envs : List Environment) : Except String MValue := do
  match frms, stck, envs with
  | [], [v], _ => pure v.get
  | (i::is)::frms', stck, envs =>
    let (frm', stck', envs') <- (exec i ((is : Frame)::frms') stck envs)
    loop frm' stck' envs'
  | [] :: frms, stck, envs => loop frms stck envs
  | _, _, _ => Except.error "Illegal end of program."

end MiniML.Machine
