import Std
import Plz.Utils

namespace MiniML
/- Syntax -/


abbrev Name := String


inductive Ty where
| TInt : Ty
| TBool : Ty
| TArrow : Ty → Ty → Ty
| Bottom : Ty -- for debugging. temporarily.
deriving BEq, DecidableEq, Inhabited, Repr
private def Ty.toString' (t : Ty) :=
match t with
| .TInt => "Int"
| .TBool => "Bool"
| .TArrow t₁ t₂ => s!"{t₁.toString'} → {t₂.toString'}"
| .Bottom => "Error"
instance : ToString Ty where
  toString t := t.toString'


inductive Expr where
| EVar : Name → Expr
| EInt : Int → Expr
| EBool : Bool → Expr
| EPlus : Expr → Expr → Expr
| ETimes : Expr → Expr → Expr
| EMinus : Expr → Expr → Expr
| EEq : Expr → Expr → Expr
| ELt : Expr → Expr → Expr
| EIf : Expr → Expr → Expr → Expr
| EFun : Name → Name → Ty → Expr → Ty → Expr
| EApply : Expr → Expr → Expr
deriving Inhabited, Repr

private def Expr.toString' (e : Expr) :=
match e with
| EVar n => s!"{n}"
| EInt i => s!"{i}"
| EBool b => if b then "⊤" else "⊥"
| EPlus e₁ e₂ => s!"{e₁.toString'} + {e₂.toString'}"
| ETimes e₁ e₂ => s!"{e₁.toString'} × {e₂.toString'}"
| EMinus e₁ e₂ => s!"{e₁.toString'} - {e₂.toString'}"
| EEq e₁ e₂ => s!"{e₁.toString'} = {e₂.toString'}"
| ELt e₁ e₂ => s!"{e₁.toString'} < {e₂.toString'}"
| EIf b e₁ e₂ => s!"if ({b.toString'}) then ({e₁.toString'}) else ({e₂.toString'})"
| EFun f x t₁ e t₂ => s!"fun {f}({x} : {t₁}) : {t₂} := {e.toString'}"
| EApply e₁ e₂  => s!"{e₁.toString'} {e₂.toString'}"
instance : ToString Expr where toString e := e.toString'

def Expr.isValue (e : Expr) :=
match e with
| .EInt _ | .EBool _ | .EFun _ _ _ _ _ => true
| _ => false

-- e[n↦s]
def Expr.subst' (e : Expr) (n : Name) (s : Expr) :=
match e with
| EVar v => if v == n then s else EVar v
| EInt i => EInt i
| EBool b => EBool b
| EPlus e₁ e₂ => EPlus (subst' e₁ n s) (subst' e₂ n s)
| ETimes e₁ e₂ => ETimes (subst' e₁ n s) (subst' e₂ n s)
| EMinus e₁ e₂ => EMinus (subst' e₁ n s) (subst' e₂ n s)
| EEq e₁ e₂ => EEq (subst' e₁ n s) (subst' e₂ n s)
| ELt e₁ e₂ => ELt (subst' e₁ n s) (subst' e₂ n s)
| EIf b e₁ e₂ => EIf (subst' b n s) (subst' e₁ n s) (subst' e₂ n s)
| e@(EFun f x t₁ e' t₂) => if (x == n) then e else (EFun f x t₁ (subst' e' n s) t₂)
| EApply e₁ e₂  => EApply (subst' e₁ n s) (subst' e₂ n s)

def Expr.subst (e : Expr) (ns : List (Name × Expr)) := Id.run do
  let mut e' <- e
  for (n, s) in ns do
    e' <- e'.subst' n s
  pure e'

/- Type checking -/

abbrev Context := Std.HashMap Name Ty
def EmptyContext : Context := Std.HashMap.empty

mutual

def Expr.type (e : Expr) (ctx : Context) : Ty := Id.run do
match e with
| EVar n => if let some t := ctx.find? n then t else .Bottom
| EInt _ => .TInt
| EBool _ => .TBool
| EPlus e₁ e₂ =>
  if (e₁.type_check ctx .TInt) ∧ (e₂.type_check ctx .TInt) then .TInt else .Bottom
| ETimes e₁ e₂ =>
  if (e₁.type_check ctx .TInt) ∧ (e₂.type_check ctx .TInt) then .TInt else .Bottom
| EMinus e₁ e₂ =>
  if (e₁.type_check ctx .TInt) ∧ (e₂.type_check ctx .TInt) then .TInt else .Bottom
| EEq e₁ e₂ =>
  if (e₁.type_check ctx .TInt) ∧ (e₂.type_check ctx .TInt) then .TBool else .Bottom
| ELt e₁ e₂ =>
  if (e₁.type_check ctx .TInt) ∧ (e₂.type_check ctx .TInt) then .TBool else .Bottom
| EIf b e₁ e₂ => {
  if b.type_check ctx .TBool then
    let ty_of_e₁ := e₁.type ctx
    if e₂.type_check ctx ty_of_e₁ then ty_of_e₁ else .Bottom
  else
    .Bottom
  }
| EFun f x ty₁ e ty₂ => {
  let ctx := ctx.insert x ty₂
  let ctx := ctx.insert f (.TArrow ty₁ ty₂)
  if (e.type_check ctx ty₂) then .TArrow ty₁ ty₂ else .Bottom
}
| EApply e₁ e₂  => {
  match e₁.type ctx with
  | .TArrow ty₁ ty₂ => if (e₂.type_check ctx ty₁) then ty₂ else .Bottom
  | _ => .Bottom
}

def Expr.type_check (e : Expr) (ctx : Context) (expected_ty : Ty) : Bool := (expected_ty ==  (e.type ctx))

end

/- Evaluate -/

abbrev Environment := Std.HashMap Name Expr
def EmptyEnv : Environment := Std.HashMap.empty

-- one-step evaluation of non-value Expr
private def Expr.eval' (e : Expr) :=
match e with
| EPlus (EInt n₁) (EInt n₂) => EInt (n₁ + n₂)
| EPlus (EInt n₁) e₂ => EPlus (EInt n₁) e₂.eval'
| EPlus e₁ e₂ => EPlus e₁.eval' e₂
| ETimes (EInt n₁) (EInt n₂) => EInt (n₁ * n₂)
| ETimes (EInt n₁) e₂ => ETimes (EInt n₁) e₂.eval'
| ETimes e₁ e₂ => ETimes e₁.eval' e₂
| EMinus (EInt n₁) (EInt n₂) => EInt (n₁ - n₂)
| EMinus (EInt n₁) e₂ => EMinus (EInt n₁) e₂.eval'
| EMinus e₁ e₂ => EMinus e₁.eval' e₂
| EEq (EInt n₁) (EInt n₂) => EBool (n₁ == n₂)
| EEq (EInt n₁) e₂ => EEq (EInt n₁) e₂.eval'
| EEq e₁ e₂ => EEq e₁.eval' e₂
| ELt (EInt n₁) (EInt n₂) => EBool (n₁ < n₂)
| ELt (EInt n₁) e₂ => ELt (EInt n₁) e₂.eval'
| ELt e₁ e₂ => ELt e₁.eval' e₂
| EIf (EBool true) e₁ _ => e₁
| EIf (EBool false) _ e₂ => e₂
| EIf b e₁ e₂ => EIf b.eval' e₁ e₂
| EApply v₁@(EFun f x _ e _) e₂ => if e₂.isValue then e.subst [(f, v₁), (x, e₂)] else (EApply v₁ e₂.eval')
| EApply e₁ e₂  => EApply e₁.eval' e₂
| EVar n => EVar n -- should be an error
| EInt i => EInt i -- should be an error
| EBool b  => EBool b -- should be an error
| f@(EFun _ _ _ _ _) => f -- should be an error

-- small-step evaluation
partial
def Expr.eval (e : Expr) :=
  loop e
where
  loop e := if e.isValue then e else loop (e.eval')

section test
open Expr
open Ty
-- factorial
-- type check:
def factorial :=  (EFun "f" "n" TInt (EIf (EEq (EVar "n") (EInt 0)) (EInt 1) (ETimes (EVar "n") (EApply (EVar "f") (EMinus (EVar "n") (EInt (1)))))) TInt)
def factorial_5 := (EApply factorial (EInt 5))
#eval Expr.type factorial EmptyContext
#eval Expr.eval factorial_5
#eval Id.run do
  let mut e <- factorial_5
  dbg_trace s!"{e}"
  while ¬e.isValue do
    e <- e.eval'
    dbg_trace s!"-> {e}"
  e
end test


/- Command -/

inductive Command where
| expression : Expr → Command
| definition : Name → Expr → Command
deriving Inhabited, Repr


def Command.eval (c : Command) (ctx : Context) (env : Environment) := Id.run do
match c with
| expression e => {
  if (e.type ctx) != Ty.Bottom then
    (some e.eval, ctx, env)
  else
    (none, ctx, env)
}
| definition n e => {
  let ty := e.type ctx
  if ty != Ty.Bottom then
    let v := e.eval
    (some v, ctx.insert n ty, env.insert n v)
  else
    (none, ctx, env)
}

end MiniML
