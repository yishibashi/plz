import Lean.Data.Parsec
namespace String

def parse? [Inhabited α] (s : String) (p : Lean.Parsec α) : Option α := Id.run do
  match p s.iter with
  | Lean.Parsec.ParseResult.success _ x => some x
  | Lean.Parsec.ParseResult.error pos err =>
    dbg_trace s!"{pos}, {err}"
    none

def dbg_parse? [Inhabited α] [ToString α] (s : String) (p : Lean.Parsec α) : String := Id.run do
  match p s.iter with
  | Lean.Parsec.ParseResult.success _ x => s!"{x}"
  | Lean.Parsec.ParseResult.error pos err => s!"{pos}, {err}"

end String

namespace Lean.Parsec

def natDigit : Parsec Nat := attempt do
  if let some nd := (<- manyChars digit).toNat? then
    return nd
  else
    fail "parse failed at `natDigit`"

partial def sepByCore (p : Parsec α) (sep : Parsec β) (xs : List α): Parsec (List α) :=
  attempt (do let _ <- sep; sepByCore p sep (xs++[<-p])) <|> pure xs
def sepBy (p : Parsec α) (sep : Parsec β) : Parsec (List α) := (do sepByCore p sep [<-p]) <|> pure []

end Lean.Parsec
