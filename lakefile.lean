import Lake
open Lake DSL

package «plz» where
  -- add package configuration options here

lean_lib «Plz» where
  -- add library configuration options here

require std from git "https://github.com/leanprover/std4" @ "v4.7.0"

@[default_target]
lean_exe «plz» where
  root := `Main
